(function () {
    "use strict";
    angular
        .module("spaBase")
        .factory('FormService',
            function ($rootScope,
                      Restangular) {

                function generateModel(form) {
                    let model = {
                        formMasterId: form.formMasterId,
                        formTypeId: 2,
                        divisionId: form.divisionId,
                        applicationModuleId: form.applicationModuleId,
                        applicationResourceId: form.applicationResourceId,
                        formName: form.form_name,
                        formTitle: form.form_title,
                        formDescription: form.form_description,
                        formMetaData: JSON.stringify(form),
                        isActive: true,
                        requestTimeStamp: new Date()
                    };

                    return model;
                }

                let addForm = function(form) {
                    let model = generateModel(form);
                    let promise = Restangular.all('formMaster').post(model);
                    return promise;
                };

                let updateForm = function(form) {
                    let model = generateModel(form);
                    let promise = Restangular.all('formMaster').customPUT(model);
                    return promise;
                };

                let getForm = function(formId) {
                    let promise = Restangular.all('formMaster').one('id', formId).get();
                    return promise;
                };

                let getForms = function(){
                    let promise = Restangular.all('formMaster').getList();
                    return promise;
                };

                let getFormsByResourceNameAndUniqueKey = function(resourceName, uniqueKey){
                    uniqueKey = (uniqueKey == undefined) ? null : uniqueKey;
                    let promise = Restangular.all('formMaster').all('formsByResource').post({resourceName: resourceName, uniqueKey: uniqueKey});
                    return promise;
                };

                let publishForm = function(formId) {
                    let promise = Restangular.all('formMaster').all('publish').one('id', formId).customPUT();
                    return promise;
                };

                let deactivateForm = function(formId) {
                    let promise = Restangular.all('formMaster').all('deactivate').one('id', formId).customPUT();
                    return promise;
                };

                return {
                    getForms: getForms,
                    getFormsByResourceNameAndUniqueKey: getFormsByResourceNameAndUniqueKey,
                    addForm: addForm,
                    updateForm: updateForm,
                    getForm: getForm,
                    publishForm: publishForm,
                    deactivateForm: deactivateForm
                };
            });
})();
