(function() {
    "use strict";
    let module = angular.module("spaBase");
    module.factory('AppModuleService',
        function(
            $rootScope,
            Restangular,
            $q) {

            let getApplicationModule = function(moduleId) {
                let appModulePromise = Restangular.all('ApplicationModule').one('id', moduleId).get();
                return appModulePromise;
            };

            let getApplicationModules = function() {
                let appModulePromise = Restangular.all('ApplicationModule').getList();
                return appModulePromise;
            };

            return {
                getApplicationModule: getApplicationModule,
                getApplicationModules: getApplicationModules
            };
        });
})();