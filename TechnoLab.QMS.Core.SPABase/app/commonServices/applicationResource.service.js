(function () {
    "use strict";
    var module = angular.module("spaBase");
    module.factory('AppResourceService',
        function ($rootScope,
                  Restangular) {

            var getApplicationResources = function (moduleId, resourceCategoryId) {
                var promise = Restangular.all('applicationResource').one('moduleId', moduleId)
                    .one('resourceCategoryId', resourceCategoryId)
                    .getList();
                return promise;
            };

            var getApplicationResourcesByModuleId = function (moduleId) {
                var promise = Restangular.all('applicationResource').one('moduleId', moduleId).getList();
                return promise;
            };


            return {
                getApplicationResources: getApplicationResources,
                getApplicationResourcesByModuleId: getApplicationResourcesByModuleId
            };
        });
})();