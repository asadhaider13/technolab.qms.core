(function() {
    "use strict";
    let module = angular.module("spaBase");
    module.factory('FormResponseService',
        function(
            $rootScope,
            Restangular
        ) {

            function generateModel(formResponse) {
                let model = {
                    formMasterId: formResponse.formMasterId,
                    resourceName: formResponse.resourceName,
                    uniqueKey: formResponse.uniqueKey,
                    responseMetaData: formResponse.responseMetaData
                };
                return model;
            }

            let saveFormResponse = function(formResponse) {
                let model = generateModel(formResponse);
                let promise = Restangular.all('formResponse').post(model);
                return promise;
            };

            let getFormResponse = function(formResponseId) {
                let promise = Restangular.all('formResponse').one('id', formResponseId).get();
                return promise;
            };

            let getFormResponses = function() {
                let promise = Restangular.all('formResponse').getList();
                return promise;
            };

            let removeFormResponse = function(formResponseId) {
                let promise = Restangular.all('formResponse').one('id', formResponseId).remove();
                return promise;
            };

            return {
                saveFormResponse: saveFormResponse,
                getFormResponse: getFormResponse,
                getFormResponses: getFormResponses,
                removeFormResponse: removeFormResponse
            };
        });
})();