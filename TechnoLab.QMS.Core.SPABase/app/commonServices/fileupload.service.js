(function() {
    "use strict";
    let module = angular.module("spaBase");
    module.factory('FileUploadService',
        function(
            $rootScope,
            Restangular) {

            let GetAllFiles = function(Id){
                let promise = Restangular.all('upload').getList();
                return promise;
            };

            let GetFileByName = function (fileName) {
                let promise = Restangular.all('upload').one('id').one('fileName', fileName).get();
                return promise;
            };

            let GetFileByField = function(field){
                let promise = Restangular.all('upload').one('download').one('field', field).get();
                return promise;
            };

            let UploadFile = function(model) {
                let promise = Restangular.all('upload').post(model);
                return promise;
            };

            let saveSupplierFiles = function(model) {
                let promise = Restangular.all('upload').all('supplierFiles').post(model);
                return promise;
            };

            let getSelectedFilesInfo = function(fileIds) {
                let promise = Restangular.all('upload').all('lstFileDetails').post({fileId: fileIds});
                return promise;
            };

            let downloadFile = function(fileId) {
                let promise = Restangular.all('upload').all('download').one('fileId', fileId).get();
                return promise;
            };

            return {
                GetAllFiles: GetAllFiles,
                GetFileByName: GetFileByName,
                GetFileByField: GetFileByField,
                UploadFile: UploadFile,
                getSelectedFilesInfo: getSelectedFilesInfo,
                saveSupplierFiles: saveSupplierFiles,
                downloadFile: downloadFile,
            };
        });
})();