; 'use strict';

angular
    .module('spaBase')
    .directive('radioButton', [
        function () {
            return {
                restrict: 'EA',
                templateUrl: 'http://localhost:37064/app/templates/radioButton.html',
                replace: true,
                scope: {
                    field: '=?'
                }
            }
        }
    ]);
