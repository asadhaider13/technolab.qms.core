﻿;'use strict';

angular
    .module('spaBase')
    .directive('qmsForm', [
        'FormService',
        function (FormService) {
            return {
                restrict: 'EA',
                templateUrl: 'http://localhost:37064/app/templates/qmsForm.html',
                replace: true,
                scope: {
                    callbacks: '=',
                    uniqueKey: '='
                },
                controller: function ($scope) {
                    let resourceName = $('div[data-customacl]').attr('data-customacl');
                    $scope.forms = [];

                    $scope.callbacks.saveResponse = function(){
                        angular.forEach($scope.forms, function (form, key) {
                            form.callbacks.saveResponse($scope.uniqueKey, resourceName);
                        })
                    };

                    FormService.getFormsByResourceNameAndUniqueKey(resourceName, $scope.uniqueKey).then(function (forms) {
                        $scope.forms = forms.data;
                        angular.forEach($scope.forms, function (form, key) {
                            form.callbacks = {};
                        })
                    });
                }
            }
        }
    ])
;