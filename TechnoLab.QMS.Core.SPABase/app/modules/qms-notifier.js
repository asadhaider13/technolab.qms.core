//Created by atir.mohiuddin on 2/16/2017.
spaBase
.factory('QmsNotifier',
        function() {
            var position = 'bottom-right';

            var notifyDTO = function(DTO){
                if(DTO.data.meta) {
                    if (DTO.data.meta.messageType == 'Success') {
                        notifyMessage_Success(DTO.data.meta.message);
                    }
                    else if (DTO.data.meta.messageType == 'Error') {
                        notifyMessage_Danger(DTO.data.meta.message);
                    }
                    else if (DTO.data.meta.messageType == 'Info') {
                        notifyMessage_Info(DTO.data.meta.message);
                    }
                    else if (DTO.data.meta.messageType == 'Warning') {
                        notifyMessage_Warning(DTO.data.meta.message);
                    }
                }
                else if(DTO.data){
                    if (DTO.data.messageType == 'Success') {
                        notifyMessage_Success(DTO.data.message);
                    }
                    else if (DTO.data.messageType == 'Error') {
                        notifyMessage_Danger(DTO.data.message);
                    }
                    else if (DTO.data.messageType == 'Info') {
                        notifyMessage_Info(DTO.data.message);
                    }
                    else if (DTO.data.messageType == 'Warning') {
                        notifyMessage_Warning(DTO.data.message);
                    }
                }
            };

            var notifyMessage_Info = function(message) {
                UIkit.notify('<a class="notify-action">x</a>' + message, {pos: 'bottom-right', status: 'info'});
            };

            var notifyMessage_Success = function(message) {
                UIkit.notify('<a class="notify-action">x</a>' + message, {pos: 'bottom-right', status: 'success'});
            };

            var notifyMessage_Warning = function(message) {
                UIkit.notify('<a class="notify-action">x</a>' + message, {pos: 'bottom-right', status: 'warning'});
            };

            var notifyMessage_Danger = function(message) {
                UIkit.notify('<a class="notify-action">x</a>' + message, {pos: 'bottom-right', status: 'danger'});
            };

            return {
                notifyDTO: notifyDTO,
                notifyMessage_Info: notifyMessage_Info,
                notifyMessage_Success: notifyMessage_Success,
                notifyMessage_Warning: notifyMessage_Warning,
                notifyMessage_Danger: notifyMessage_Danger,
            };
        });
