; 'use strict';

angular
    .module('spaBase')
    .directive('textArea', [
        function () {
            return {
                restrict: 'EA',
                templateUrl: 'http://localhost:37064/app/templates/textArea.html',
                replace: true,
                scope: {
                    field: '=?'
                }
            }
        }
    ]);