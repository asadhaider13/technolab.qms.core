; 'use strict';

angular
    .module('spaBase')
    .directive('textField', [
        function () {
            return {
                restrict: 'EA',
                templateUrl: 'http://localhost:37064/app/templates/textField.html',
                replace: true,
                scope: {
                    field: '=?'
                },
                controller: function ($scope) {
                    console.log($scope.field);
                }
            }
        }
    ]);