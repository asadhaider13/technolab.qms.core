﻿;'use strict';

angular
    .module('spaBase')
    .directive('doctype', [
        '$rootScope',
        '$filter',
        'FormService',
        'FormResponseService',
        'HelperService',
        function ($rootScope, $filter, FormService, FormResponseService, HelperService) {
            return {
                restrict: 'E',
                templateUrl: 'http://localhost:37064/app/templates/doctype.html',
                replace: true,
                scope: {
                    settings: '=?',
                    callbacks: '='
                },
                controller: function ($scope) {
                    $scope.formMasterId = $scope.settings.formMasterId;
                    $scope.responseMetaData = JSON.parse($scope.settings.responseMetaData);
                    $scope.settings = JSON.parse($scope.settings.formMetaData);

                    const FindByValue = function (array, value) {
                        const found = $filter('filter')(array, {field_name: value}, true);
                        if (found.length) {
                            return found[0];
                        }
                        return null;
                    };

                    if ($scope.settings.conditions) {
                        for (let conditionCounter = 0; conditionCounter < $scope.settings.conditions.length; conditionCounter++) {
                            const condition = $scope.settings.conditions[conditionCounter];

                            angular.forEach($scope.settings.form_fields, function (field, key) {

                                if (field.field_name == condition.leftHandField) {

                                    const watchString = 'settings.form_fields[' + key + ']';
                                    $scope.$watch(watchString, function (newValue, oldValue) {
                                        let leftHandValue = newValue.field_data.field_value;
                                        let condition = null;

                                        angular.forEach($scope.settings.conditions, function (value, key) {
                                            if (value.leftHandField == newValue.field_name) {
                                                condition = value;
                                            }
                                        });

                                        let result = true;
                                        let value = null;

                                        if (condition.valueType === 'value') {
                                            value = condition.value;
                                        }
                                        else {
                                            const valueField = FindByValue($scope.settings.form_fields, condition.valueField);
                                            value = valueField.field_data.field_value;
                                        }

                                        if (condition.state == 'contains') {
                                            result = leftHandValue.includes(value);
                                        } else if (condition.state == 'notContains') {
                                            result = !leftHandValue.includes(value);
                                        } else if (condition.state == 'startsWith') {
                                            result = leftHandValue.startsWith(value);
                                        } else if (condition.state == 'notStartsWith') {
                                            result = !leftHandValue.startsWith(value);
                                        } else if (condition.state == 'endsWith') {
                                            result = leftHandValue.endsWith(value);
                                        } else if (condition.state == 'notEndsWith') {
                                            result = !leftHandValue.endsWith(value);
                                        } else if (condition.state == 'isEmpty') {
                                            result = (!value);
                                        } else if (condition.state == 'IsFilled') {
                                            result = !!(value);
                                        } else {
                                            const evalString = 'leftHandValue ' + condition.state + ' rightHandValue';
                                            $scope.leftHandValue = leftHandValue;
                                            $scope.rightHandValue = value;
                                            result = $scope.$eval(evalString);
                                        }

                                        angular.forEach($scope.settings.form_fields, function (field, key) {
                                            if (field.field_name == condition.rightHandField) {

                                                if (condition.do == 'show') {
                                                    field.field_data.visible = result;
                                                } else if (condition.do == 'hide') {
                                                    field.field_data.visible = !result;
                                                } else if (condition.do == 'enable') {
                                                    field.field_data.enabled = result;
                                                } else if (condition.do == 'disable') {
                                                    field.field_data.enabled = !result;
                                                }

                                                if (field.children) {
                                                    angular.forEach(field.children, function (childrenField, key) {
                                                        angular.forEach($scope.settings.form_fields, function (field, key) {
                                                            if (field.field_name == childrenField) {
                                                                if (condition.do == 'show') {
                                                                    field.field_data.visible = result;
                                                                } else if (condition.do == 'hide') {
                                                                    field.field_data.visible = !result;
                                                                } else if (condition.do == 'enable') {
                                                                    field.field_data.enabled = result;
                                                                } else if (condition.do == 'disable') {
                                                                    field.field_data.enabled = !result;
                                                                }
                                                            }
                                                        });
                                                    });
                                                }
                                            }
                                        });
                                    }, true);
                                }
                            });
                        }
                    }

                    angular.forEach($scope.settings.form_fields, function (field, key) {
                        let html = '';
                        const newScope = $rootScope.$new();

                        angular.forEach($scope.responseMetaData, function (fieldResponse, key) {
                            if (fieldResponse.field_name == field.field_name)
                                field.field_data.field_value = fieldResponse.field_value;
                        });

                        newScope.field = field;

                        if (field.field_type === 'textField') {
                            html = '<text-field field="field"></text-field>';
                        }
                        else if (field.field_type === 'textArea') {
                            html = '<text-area field="field"></text-area>';
                        }
                        else if (field.field_type === 'radioButton') {
                            html = '<radio-button field="field"></radio-button>';
                        }
                        else if (field.field_type === 'checkBox') {
                            html = '<check-box field="field"></check-box>';
                        }
                        else if (field.field_type === 'select') {
                            html = '<select-field field="field"></select-field>';
                        }
                        HelperService.AppendHTML('formPreview', html, newScope);
                    });

                    $("#formPreview").attr("id", $scope.settings.form_name);

                    $scope.callbacks.saveResponse = function (uniqueKey, resourceName) {
                        let response = [];
                        angular.forEach($scope.settings.form_fields, function (field, key) {
                            response.push({
                                field_name: field.field_name,
                                field_value: field.field_data.field_value
                            })
                        });

                        // let formMasterId = $scope.settings.formMasterId;

                        let model = {
                            formMasterId: $scope.formMasterId,
                            uniqueKey: uniqueKey,
                            resourceName: resourceName,
                            responseMetaData: JSON.stringify(response)
                        };

                        FormResponseService.saveFormResponse(model).then(function (result) {
                            console.log(result);
                        });
                    }
                }
            }
        }
    ])
;