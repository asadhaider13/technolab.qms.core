; 'use strict';

angular
    .module('spaBase')
    .directive('selectField', [
        function () {
            return {
                restrict: 'EA',
                templateUrl: 'http://localhost:37064/app/templates/selectField.html',
                replace: true,
                scope: {
                    field: '=?'
                },
                controller: function ($scope) {
                    $scope.optionsConfigurations = {
                        maxItems: ($scope.field.field_data.multiSelect) ? null : 1,
                        valueField: 'value',
                        labelField: 'text',
                        create: false,
                        placeholder: 'Choose...'
                    };
                }
            }
        }
    ]);