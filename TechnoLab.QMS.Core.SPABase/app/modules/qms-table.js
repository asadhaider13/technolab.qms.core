/**
 * Created by atir.mohiuddin on 2/18/2017.
 */
spaBase
    .directive('qmsTable', function () {
        return {
            restrict: 'EA', //E = element, A = attribute, C = class, M = comment
            scope: {
                //@ reads the attribute value, = provides two-way binding, & works with functions
                records: '=',
                columns: '=',
                actionAttr: '=',
                actionSet: '=',
                actionCallback: '&',
                paginationCallback: '&',
                aclPrefix: '@'
            },
            templateUrl: 'http://localhost:37064/app/templates/qmsTable.html',
            controller: function ($scope, $translate, $filter) {
                $scope.selectize_a = '10';

                function bindTable() {
                    $scope.results = [];
                    if ($scope.records.data) {
                        for (let i = 0; i < $scope.records.data.length; i++) {
                            $scope.mArr = {};
                            let index = 0;
                            for (let keyName in $scope.columns[0]) {
                                let key = keyName;


                                if (index == 0) {
                                    let tempKey = $scope.columns[0][keyName];
                                    let value = $scope.records.data[i][tempKey];
                                    //if ($scope.records.data[i][tempKey]) {
                                    //let new_key = $scope.columns[0][keyName];
                                    $scope.mArr[keyName] = value;
                                    //}
                                }
                                else {
                                    let value = $scope.records.data[i][keyName];
                                    value = (!value) ? '' : value;
                                    if ($filter('checkTimeStamp')(value)) value = $filter('qmsDateFormat')(value);

                                    let new_key = $scope.columns[0][keyName];
                                    $scope.mArr[new_key] = value;
                                    //}
                                }

                                index++;

                            }
                            $scope.results.push($scope.mArr);
                        }
                        //console.log($scope.results);
                    }
                }

                if ($scope.records.data) {
                    $scope.setRecordCount();
                    bindTable();
                }

                $scope.$watch('records', function () {
                    if ($scope.records.data) {
                        $scope.setRecordCount();
                        bindTable();
                    }

                }, true);

                $scope.pageChanged = function (oldPageNumber, newPageNumber) {
                    $scope.paginationCallback({currentpage: oldPageNumber, nextpage: newPageNumber, pagesize: $scope.pageSize});
                };

                $scope.pageSize = 10;
                $scope.totalRecords = 0;

                $scope.setRecordCount = function () {
                    if ($scope.records.data) {
                        //console.log('pageSize : ' + $scope.records.data.meta.pageSize);
                        $scope.pageSize = $scope.records.data.meta.pageSize;
                    }

                    if ($scope.records.data) {
                        //console.log('totalRecords : ' + $scope.records.data.meta.totalRecords);
                        $scope.totalRecords = $scope.records.data.meta.totalRecords;
                    }
                }
                $scope.setRecordCount();

                $scope.selectize_a_data = {
                    options: [
                        {
                            id: 1,
                            title: "10",
                            value: "10",
                            parent_id: 1
                        },
                        {
                            id: 2,
                            title: "25",
                            value: "25",
                            parent_id: 1
                        },
                        {
                            id: 3,
                            title: "50",
                            value: "50",
                            parent_id: 1
                        },
                        {
                            id: 4,
                            title: "100",
                            value: "100",
                            parent_id: 1
                        }
                    ]
                };

                $scope.selectize_a_config = {
                    create: false,
                    maxItems: 1,
                    placeholder: 'Page Size',
                    optgroupField: 'parent_id',
                    optgroupLabelField: 'title',
                    optgroupValueField: 'ogid',
                    valueField: 'value',
                    labelField: 'title',
                    searchField: 'title',
                    onChange: function (value) {
                        $scope.paginationCallback({currentpage: 1, nextpage: 1, pagesize: value});
                    }
                };

            },
            link: function ($scope, element, attrs) {
            } //DOM manipulation
        }
    })
    .directive("acl", function ($compile) {
        return {
            scope: {
                //@ reads the attribute value, = provides two-way binding, & works with functions
                columns: '=',
            },
            controller: 'aclController',
            compile: function (el) {

                return function (scope, el, attr) {
                    //let attributes = scope.$eval(attr.acl);
                    let attributes = scope.columns;
                    //console.log(attributes);

                    for (let keyName in attributes[0]) {
                        let key = keyName;
                        let value = attributes[0][keyName];

                        if (value == attr.reference) {
                            el.attr(key, '');
                            //el.removeAttribute('acl');
                        }
                    }
                };
            }
        };
    })
    .controller("aclController", function ($scope) {

    });