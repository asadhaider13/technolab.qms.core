; 'use strict';

angular
    .module('spaBase')
    .directive('checkBox', [
        function () {
            return {
                restrict: 'EA',
                templateUrl: 'http://localhost:37064/app/templates/checkBox.html',
                replace: true,
                scope: {
                    field: '=?'
                }
            }
        }
    ]);
