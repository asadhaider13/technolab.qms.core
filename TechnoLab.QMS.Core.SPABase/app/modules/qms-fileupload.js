/**
 * Created by atir.mohiuddin on 2/18/2017.
 */
spaBase
    .directive('qmsFileUpload', function () {
        return {
            restrict: 'EA', //E = element, A = attribute, C = class, M = comment
            scope: {
                selected: '=',
                isValidity: '=',
                isDescription: '=?'
            },
            templateUrl: 'http://localhost:37064/app/templates/qmsFileUploadButtons.tpl.html',
            controller: function ($scope, $translate, $kWindow) {

                $scope.title = "File Upload";
                $scope.isDescription = ($scope.isDescription == null) ? true: $scope.isDescription;

                $scope.openUploadWindow = function () {
                    let windowInstance = $kWindow.open({
                        options: {
                            modal: true,
                            title: $scope.title,
                            resizable: true,
                            height: 500,
                            width: 1200,
                            visible: true
                        },
                        templateUrl: 'http://localhost:37064/app/templates/qmsFileUpload.tpl.html',
                        controller: 'fileUploadCtrl',
                        resolve: {
                            message: function () {
                                return $scope.content;
                            },
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_iCheck',
                                    'http://localhost:37064/app/commonServices/fileupload.service.js',
                                ]);
                            }],
                            isValidity: $scope.isValidity,
                            isDescription: $scope.isDescription,
                        }
                    });

                    windowInstance.result.then(function (result) {
                        $scope.selected.push(result);
                        console.log($scope.selected);
                    });
                };
                $scope.openViewWindow = function () {
                    let windowInstance = $kWindow.open({
                        options: {
                            modal: true,
                            title: $scope.title,
                            resizable: true,
                            height: 500,
                            width: 1200,
                            visible: true
                        },
                        templateUrl: 'http://localhost:37064/app/templates/qmsFileView.tpl.html',
                        controller: 'fileViewCtrl',
                        resolve: {
                            message: function () {
                                return $scope.fileId;
                            },
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_iCheck',
                                    'http://localhost:37064/app/commonServices/fileupload.service.js',
                                ]);
                            }],
                            selectedFiles: {fileIds: $scope.selected}
                        }
                    });


                    windowInstance.result.then(function (result) {
                        console.log(result);
                        $scope.selected = result;
                    });
                };
            }
        }
    })

    .controller('fileUploadCtrl', [
        '$rootScope',
        '$scope',
        '$http',
        'FileUploader',
        'isValidity',
        'isDescription',
        'FileUploadService',
        '$localStorage',
        '$windowInstance',
        '$timeout',
        function ($rootScope, $scope, $http, FileUploader, isValidity, isDescription, FileUploadService, $localStorage, $windowInstance, $timeout) {
            let uploader;

            $scope.dtStart = new Date().toLocaleDateString();
            $scope.dtEnd = new Date().toLocaleDateString();
            $scope.isValidity = isValidity;
            $scope.isDescription = isDescription;
            $scope.description = '';

            uploader = $scope.uploader = new FileUploader({
                url: 'http://localhost:51149/api/Upload',
                formData: [
                    {Description: $scope.description},
                    {IsValidity: $scope.isValidity},
                    {ValidityStart: $scope.dtStart},
                    {ValidityEnd: $scope.dtEnd}
                ],
                headers: {Authorization: 'Bearer ' + $localStorage['idToken']}
            });

            $scope.result = [];
            $scope.actionKeyAttr = 'Action(s)';

            $scope.columns = [{
                'actionRefId': 'fileMasterId',
                'fileName': 'File Name',
                'uploadedById': 'Uploaded By',
                'uploadedOn': 'Uploaded On',
                'validityEnd': 'Validity Start',
                'validityStart': 'Validity End'
            }];

            $scope.actions = {
                'select': 'file_upload',
                'download': 'file_download'
            };

            $scope.actionCallback = function (value, action) {
                if (action == 'select') {
                    $windowInstance.close(value);
                }
                if (action == 'download') {
                    FileUploadService.downloadFile(value).then(function (response) {
                        let a = document.createElement('a');
                        a.href = URL.createObjectURL(new Blob([response.data]));
                        a.download = 'PleaseFixThisError.txt';
                        a.target = '_blank';
                        a.click();
                    });
                }
            };

            $scope.paginationCallback = function (currentpage, nextpage, pagesize) {
            };

            FileUploadService.GetAllFiles().then(function (model) {
                $scope.result = model;
            });

            uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
            };
            uploader.onAfterAddingFile = function (fileItem) {
            };
            uploader.onAfterAddingAll = function (addedFileItems) {
            };
            uploader.onBeforeUploadItem = function (item) {
            };
            uploader.onProgressItem = function (fileItem, progress) {
            };
            uploader.onProgressAll = function (progress) {
            };
            uploader.onSuccessItem = function (fileItem, response, status, headers) {
            };
            uploader.onErrorItem = function (fileItem, response, status, headers) {
            };
            uploader.onCancelItem = function (fileItem, response, status, headers) {
            };
            uploader.onCompleteItem = function (fileItem, response, status, headers) {
                $windowInstance.close(response);
            };
            uploader.onCompleteAll = function () {
            };
        }
    ])

    .controller('fileViewCtrl', [
        '$rootScope',
        '$scope',
        'selectedFiles',
        'FileUploadService',
        '$windowInstance',
        function ($rootScope, $scope, selectedFiles, FileUploadService, $windowInstance) {

            $scope.files = [];
            $scope.actionKeyAttr = 'Action(s)';

            $scope.columns = [{
                'actionRefId': 'fileMasterId', //This key is fixed value is based upon what actionRefId is required
                'fileName': 'File Name', //Key : denotes json result column map --- Value : Table head string should be used normally with translation key
                'uploadedById': 'Uploaded By',
                'uploadedOn': 'Uploaded On',
                'validityEnd': 'Validity Start',
                'validityStart': 'Validity End'
            }];

            $scope.actions = {
                'cancel': 'cancel',
                'download': 'file_download'
            };

            $scope.actionCallback = function (value, action) {
                if (action == 'cancel') {
                    angular.forEach($scope.files.data, function (file, key) {
                        if (value == file.fileMasterId) {
                            $scope.files.data.splice($scope.files.data.indexOf(file), 1);
                        }
                    });
                }
                if (action == 'download') {
                    FileUploadService.downloadFile(value).then(function (response) {
                        window.open(response);
                    });
                }
            };

            $scope.paginationCallback = function (currentpage, nextpage, pagesize) {
            };

            $scope.SaveChanges = function () {
                let fileIds = [];
                angular.forEach($scope.files.data, function (file, key) {
                    fileIds.push(file.fileMasterId);
                });
                $windowInstance.close(fileIds);
            };

            FileUploadService.getSelectedFilesInfo(selectedFiles.fileIds).then(function (result) {
                $scope.files = result;
            });
        }
    ])
;

