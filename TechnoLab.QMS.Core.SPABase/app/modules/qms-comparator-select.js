/**
 * Created by atir.mohiuddin on 2/18/2017.
 */
spaBase
    .directive('qmsComparatorSelect', function () {
        return {
            restrict: 'EA', //E = element, A = attribute, C = class, M = comment
            scope: {
                //@ reads the attribute value, = provides two-way binding, & works with functions
                records: '=',
                selectedArr: '=',
                columnName: '=',

            },
            templateUrl: 'http://localhost:37064/app/templates/qmsComparatorSelect.tpl.html',
            controller: function ($scope, $translate) {
                $scope.Selector2 = [];
                $scope.selectedArr = [];
                if($scope.records) {
                    $scope.Selector1 = $scope.records.data;
                }

                $scope.$watch('records', function() {
                    if($scope.records) {
                        $scope.Selector1 = $scope.records.data;
                    }
                });

                $scope.Select = function (Selector1, index){
                    $scope.Selector2.push(Selector1);
                    $scope.Selector1.splice(index, 1);
                    $scope.selectedArr = $scope.Selector2;
                };

                $scope.deSelect = function (Selector2, index){
                    $scope.Selector1.push(Selector2);
                    $scope.Selector2.splice(index, 1);
                    $scope.selectedArr = $scope.Selector2;
                };

                $scope.selectAll = function (){
                    for(var i = 0; i < $scope.Selector2.length; i++)
                    {
                        $scope.Selector1.push($scope.Selector2[i]);
                    }
                    $scope.Selector2 = $scope.selectedArr = [];
                    $scope.Selector2 = $scope.selectedArr = $scope.Selector1;
                    $scope.Selector1 = [];
                };

                $scope.deSelectAll = function (){
                    for(var i = 0; i < $scope.Selector1.length; i++)
                    {
                        $scope.Selector2.push($scope.Selector1[i]);
                    }
                    $scope.Selector1 = $scope.Selector2;
                    $scope.selectedArr = [];
                    $scope.Selector2 = [];
                };

            },
            link: function ($scope, element, attrs) { } //DOM manipulation
        }
    });