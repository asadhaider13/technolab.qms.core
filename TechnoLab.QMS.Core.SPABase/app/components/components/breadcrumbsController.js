angular
    .module('spaBase')
    .controller('breadcrumbsCtrl', [
        '$scope',
        '$rootScope',
        function ($scope,$rootScope) {
            $rootScope.toBarActive = true;
        }
    ]);