/*
*  Author : Syed Atir Mohiuddin
*  Dated : 1/1/2017
*  Revision : 1.0
*  Altair Admin (AngularJS)
*  Automated tasks ( http://gulpjs.com/ )
*/

var gulp = require('gulp'),
    plugins = require("gulp-load-plugins")({
        pattern: ['gulp-*', 'gulp.*', '*'],
        replaceString: /\bgulp[\-.]/
    }),
	hosting = require("./gulpInformation.json");

// browser sync
var bs_angular = require('browser-sync').create('qms_microservice');

// URL Adjustment CSS (https://www.npmjs.com/package/gulp-css-url-adjuster)
var urlAdjuster = require('gulp-css-url-adjuster');

// chalk error
var chalk = require('chalk');
var chalk_error = chalk.bold.red;

// get app version version
var pjson = require('./package.json');
var version = pjson.version;

var paths = {
    webroot: "./" + hosting.webroot + "/"
};

// 1. -------------------- MINIFY/CONCATENATE JS FILES --------------------

gulp.task('app_js', function () {
    return gulp.src([
        "app/app.js",
        "app/app.controller.js",
        "app/app.factory.js",
        "app/app.directive.js",
        "app/app.service.js",
        "app/app.states.js",
        "app/app.filters.js",
        "app/app.oc_lazy_load.js",
        "app/app.authinterceptors.js"
    ])
        .pipe(plugins.concat('app.js'))
        .on('error', function (err) {
            console.log(chalk_error(err.message));
            this.emit('end');
        })
        .pipe(gulp.dest('dist/app/'))
        .pipe(plugins.uglify({
            mangle: true
        }))
        .pipe(plugins.rename('app.min.js'))
        .pipe(plugins.size({
            showFiles: true
        }))
        .pipe(gulp.dest('dist/app/'));
});

// commmon
gulp.task('common_js', function () {
    return gulp.src([
        "bower_components/jquery/dist/jquery.js",
        "bower_components/modernizr/modernizr.js",
        // moment
        "bower_components/moment/moment.js",
        // fastclick (touch devices)
        "bower_components/fastclick/lib/fastclick.js",
        // custom scrollbar
        "bower_components/jquery.scrollbar/jquery.scrollbar.js",
        // create easing functions from cubic-bezier co-ordinates
        "bower_components/jquery-bez/jquery.bez.min.js",
        // Get the actual width/height of invisible DOM elements with jQuery
        "bower_components/jquery.actual/jquery.actual.js",
        // waypoints
        "bower_components/waypoints/lib/jquery.waypoints.js",
        // velocityjs (animation)
        "bower_components/velocity/velocity.js",
        "bower_components/velocity/velocity.ui.js",
        // advanced cross-browser ellipsis
        "bower_components/jquery.dotdotdot/src/js/jquery.dotdotdot.js",
        // hammerjs
        "bower_components/hammerjs/hammer.js",
        // scrollbar width
        "assets/js/custom/jquery.scrollbarWidth.js",
        // jquery.debouncedresize
        "bower_components/jquery.debouncedresize/js/jquery.debouncedresize.js",
        // screenfull
        "bower_components/screenfull/dist/screenfull.js",
        // waves
        "bower_components/Waves/dist/waves.min.js",
        // loadash
        "bower_components/lodash/dist/lodash.js"
    ])
        .pipe(plugins.concat('common.js'))
        .on('error', function(err) {
            console.log(chalk_error(err.message));
            this.emit('end');
        })
        .pipe(gulp.dest('assets/js/'))
        .pipe(plugins.uglify({
            mangle: true
        }))
        .pipe(plugins.rename('common.min.js'))
        .pipe(plugins.size({
            showFiles: true
        }))
        .pipe(gulp.dest('assets/js/'));
});

//[+] Angular Components //
gulp.task('angularComponents_js', function () {
    return gulp.src([
        // Angular Main
        "bower_components/angular/angular.js",
        // Angular Sanitize
        "bower_components/angular-sanitize/angular-sanitize.js",
        // Angular Animate
        "bower_components/angular-animate/angular-animate.js",
        // Angular Ui Router
        "bower_components/angular-ui-router/release/angular-ui-router.js",
		// Ng Route
		"bower_components/angular-route/angular-route.js",
        // Oc Lazy Load
        "bower_components/oclazyload/dist/ocLazyLoad.js",
        // Angular Retina
        "app/modules/angular-retina.js",
        // BreadCrumbs
        "bower_components/angular-breadcrumb/dist/angular-breadcrumb.js",
        // Restangular 1.5.2
        "bower_components/restangular/dist/restangular.js",
        // Angular Satellizer
        "app/modules/satellizer/satellizer.js",
        // ngStorage adding 0.3
        "bower_components/ngstorage/ngStorage.js",
		//OAuth https://github.com/michaelschnyder/oidc-angular
		"app/modules/oidc-angular.js",
		//angular-base64
		"bower_components/angular-base64/angular-base64.js",
		//Angular Translate
		"bower_components/angular-translate/angular-translate.js",
		//Kendo angular directive
		"app/modules/kendo.all.min.js",
		//Kendo Custom Window
		"app/modules/angular-kendo-window.js",
        //File Upload
        "app/modules/angular-file-upload.js",
        //Dir Pagination
        "bower_components/angularUtils-pagination/dirPagination.js",

        //Filter
        "bower_components/angular-filter/dist/angular-filter.js"
    ])
        .pipe(plugins.concat('angularComponents.js'))
        .on('error', function (err) {
            console.log(chalk_error(err.message));
            this.emit('end');
        })
        .pipe(gulp.dest('dist/angularComponents/'))
        .pipe(plugins.uglify({
            mangle: true
        }))
        .pipe(plugins.rename('angularComponents.min.js'))
        .pipe(plugins.size({
            showFiles: true
        }))
        .pipe(gulp.dest('dist/angularComponents/'));
});

// custom uikit
gulp.task('uikit_js', function () {
    return gulp.src([
        // uikit core
        "bower_components/uikit/js/uikit.js",
        // uikit components
        "bower_components/uikit/js/components/accordion.js",
        "bower_components/uikit/js/components/autocomplete.js",
        "assets/js/custom/uikit_datepicker.js",
        "bower_components/uikit/js/components/form-password.js",
        "bower_components/uikit/js/components/form-select.js",
        "bower_components/uikit/js/components/grid.js",
        "bower_components/uikit/js/components/lightbox.js",
        "bower_components/uikit/js/components/nestable.js",
        "bower_components/uikit/js/components/notify.js",
        "bower_components/uikit/js/components/slideshow.js",
        "bower_components/uikit/js/components/sortable.js",
        "bower_components/uikit/js/components/sticky.js",
        "bower_components/uikit/js/components/tooltip.js",
        "assets/js/custom/uikit_timepicker.js",
        "bower_components/uikit/js/components/upload.js",
        "assets/js/custom/uikit_beforeready.js"
    ])
        .pipe(plugins.concat('uikit_custom.js'))
        .pipe(gulp.dest('assets/js/'))
        .pipe(plugins.uglify({
            mangle: true
        }))
        .pipe(plugins.rename('uikit_custom.min.js'))
        .pipe(plugins.size({
            showFiles: true
        }))
        .pipe(gulp.dest('assets/js/'));
});

// common/custom functions
gulp.task('custom_js_minify', function () {
    return gulp.src([
        'assets/js/custom/*.js',
        '!assets/js/**/*.min.js'
    ])
        .pipe(plugins.uglify({
            mangle: true
        }))
        .pipe(plugins.rename({
            extname: ".min.js"
        }))
        .pipe(gulp.dest(function(file) {
            return file.base;
        }));
});

// -------------------- LESS TO CSS --------------------

// main styles
gulp.task('less_main', function() {
    return gulp.src('assets/less/main.less')
        .pipe(plugins.less())
        .on('error', function(err) {
            console.log(chalk_error(err.message));
            this.emit('end');
        })
        .pipe(plugins.autoprefixer({
            browsers: ['> 5%','last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('assets/css'))
        .pipe(bs_angular.stream())
        .pipe(plugins.minifyCss({
            keepSpecialComments: 0,
            advanced: false
        }))
        .pipe(plugins.rename('main.min.css'))
        .pipe(gulp.dest('assets/css'));
});

// main style URL Re-Route
gulp.task('url_rewrite', function() {
    return gulp.src('assets/css/main.min.css').
  pipe(urlAdjuster({
    replace:  ['../',''],
  }))
  .pipe(gulp.dest('assets/css/main.reroute.min.css'));
});

// -------------------- MINIFY JSON --------------------

gulp.task('json_minify', function() {
    return gulp.src([
            'data/*.json',
            '!data/*.min.json'
        ])
        .pipe(plugins.jsonminify())
        .on('error', function(err) {
            console.log(chalk_error(err.message));
            this.emit('end');
        })
        .pipe(plugins.rename({
            extname: ".min.json"
        }))
        .pipe(gulp.dest('data/'));
});

// -------------------- BROWSER SYNC http://www.browsersync.io/docs/ --------------------
gulp.task('browser-sync', ['default'], function() {

    bs_angular.init({
        // http://www.browsersync.io/docs/options/#option-host
        host: "10.0.0.188",
        // http://www.browsersync.io/docs/options/#option-proxy
        proxy: "qms_microservice.local",
        // http://www.browsersync.io/docs/options/#option-port
        port: 3022,
        // http://www.browsersync.io/docs/options/#option-notify
        notify: true,
        ui: {
            port: 3021
        }
    });

    gulp.watch([
        'assets/less/**/*.less',
    ],['less_main']);

    gulp.watch([
        'index.html',
        'app/**/*',
        '!app/**/*.min.js'
    ]).on('change', bs_angular.reload);

});


// generate user theme
gulp.task('less_my_theme', function() {
    return gulp.src('assets/less/themes/my_theme.less')
        .pipe(plugins.less())
        .on('error', function(err) {
            console.log(chalk_error(err.message));
            this.emit('end');
        })
        .pipe(plugins.autoprefixer({
            browsers: ['> 5%','last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('assets/css/'))
        .pipe(plugins.minifyCss({
            keepSpecialComments: 0,
            advanced: false
        }))
        .pipe(plugins.rename('my_theme.min.css'))
        .pipe(gulp.dest('assets/css/'));
});

// -------------------- WATCH TASK -------------------------------------------
// Watch Files For Changes
gulp.task('watch', function () {
    //gulp.watch("app/app*.js", ['app_js']);
	gulp.watch([
        'index.html',
        'app/**/*',
        '!app/**/*.min.js'
    ]);
});

// -------------------- DEFAULT TASK ----------------------
gulp.task('default', function(callback) {
	console.log("");
	console.log("**************************************************************");
	console.log("* Author : " + pjson.author + " *");
	console.log("* Version : " + pjson.version + " *");
	console.log("* Project : " + pjson.name + " *");
	console.log("* Description : " + pjson.description + " *");
	console.log("**************************************************************");
	console.log("");
	plugins.runSequence(
        ['app_js', 'common_js', 'angularComponents_js', 'custom_js_minify','uikit_js','less_main','json_minify', 'url_rewrite'],
        callback
    );
});

