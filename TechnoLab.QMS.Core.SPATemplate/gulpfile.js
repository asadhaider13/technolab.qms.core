/*
 *  Author : Syed Atir Mohiuddin
 *  Dated : 1/1/2017
 *  Revision : 1.0
 *  Altair Admin (AngularJS)
 *  Automated tasks ( http://gulpjs.com/ )
 */

var gulp = require('gulp'),
    plugins = require("gulp-load-plugins")({
        pattern: ['gulp-*', 'gulp.*', '*'],
        replaceString: /\bgulp[\-.]/
    }),
    hosting = require("./gulpInformation.json");

// browser sync
var bs_angular = require('browser-sync').create('qms_microservice');

// URL Adjustment CSS (https://www.npmjs.com/package/gulp-css-url-adjuster)
var urlAdjuster = require('gulp-css-url-adjuster');

// chalk error
var chalk = require('chalk');
var chalk_error = chalk.bold.red;

// get altair version
var pjson = require('./package.json');
var version = pjson.version;

// run sequence
//var runSequence = require('run-sequence');

var paths = {
    webroot: "./" + hosting.webroot + "/"
};

gulp.task('app_js', function () {
    return gulp.src([
        "app/app.js",
        "app/app.controller.js",
        "app/app.factory.js",
        "app/app.directive.js",
        "app/app.service.js",
        "app/app.states.js",
        "app/app.filters.js",
        "app/app.oc_lazy_load.js",
        "app/app.authinterceptors.js"
    ])
        .pipe(plugins.concat('app.js'))
        .on('error', function (err) {
            console.log(chalk_error(err.message));
            this.emit('end');
        })
        .pipe(gulp.dest('dist/app/'))
        .pipe(plugins.uglify({
            mangle: true
        }))
        .pipe(plugins.rename('app.min.js'))
        .pipe(plugins.size({
            showFiles: true
        }))
        .pipe(gulp.dest('dist/app/'));
});

gulp.task('watch', function () {
    //gulp.watch("app/app*.js", ['app_js']);
    gulp.watch([
        'index.html',
        'app/**/*',
        '!app/**/*.min.js'
    ]);
});

// -------------------- BROWSER SYNC http://www.browsersync.io/docs/ --------------------
gulp.task('browser-sync', ['default'], function() {

    bs_angular.init({
        // http://www.browsersync.io/docs/options/#option-host
        host: "10.14.14.78",
        // http://www.browsersync.io/docs/options/#option-proxy
        proxy: "qms_microservice.local",
        // http://www.browsersync.io/docs/options/#option-port
        port: 8080,
        // http://www.browsersync.io/docs/options/#option-notify
        notify: true,
        ui: {
            port: 8080
        }
    });

    gulp.watch([
        'assets/less/**/*.less',
    ],['less_main']);

    gulp.watch([
        'index.html',
        'app/**/*',
        '!app/**/*.min.js'
    ]).on('change', bs_angular.reload);

});

// -------------------- DEFAULT TASK ----------------------
gulp.task('default', function(callback) {
    console.log("");
    console.log("**************************************************************");
    console.log("* Author : " + pjson.author + " *");
    console.log("* Version : " + pjson.version + " *");
    console.log("* Project : " + pjson.name + " *");
    console.log("* Description : " + pjson.description + " *");
    console.log("**************************************************************");
    console.log("");
    plugins.runSequence(
        ['app_js','browser-sync'],
        callback
    );
});