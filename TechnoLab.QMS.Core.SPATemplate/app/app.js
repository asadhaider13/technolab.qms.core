/*
 *  Altair Admin AngularJS
 */
;"use strict";

var spaBase = angular.module('spaBase', [
    'ui.router',
    'oc.lazyLoad',
    'ngSanitize',
    'ngAnimate',
    'ngRetina',
    'ngStorage',
    'ncy-angular-breadcrumb',
    'ConsoleLogger',
    'satellizer',
    'ngRoute',
    'restangular',
    'oidc-angular',
    'pascalprecht.translate',
    'kendo.window',
    'kendo.directives',
]);

spaBase.config(function ($translateProvider) {
    $translateProvider.useSanitizeValueStrategy('sanitize');
});

spaBase.config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        'https://www.youtube.com/**',
        'https://w.soundcloud.com/**'
    ]);
});

// breadcrumbs
spaBase.config(function ($breadcrumbProvider) {
    $breadcrumbProvider.setOptions({
        prefixStateName: 'restricted.dashboard',
        templateUrl: 'app/templates/breadcrumbs.tpl.html'
    });
});

spaBase.config(['$authProvider', function ($authProvider) {

    $authProvider.configure({
        basePath: 'http://192.168.8.103:5000/identity',
        clientId: 'Test',
        responseType: 'id_token token',
        scope: 'openid profile email',
        tenant: 'sgs',
        apiUrl: 'http://23.99.110.102:55028/api/',
    })
}]);

//Restangular configuration
spaBase.config(['RestangularProvider', function (RestangularProvider) {
    //TODO: Configure base url
    RestangularProvider.setBaseUrl("http://localhost:55028/api/");
    RestangularProvider.setFullResponse(true);
}]);


/* Run Block */
spaBase
    .run([
        '$rootScope',
        '$state',
        '$stateParams',
        '$http',
        '$window',
        '$timeout',
        'preloaders',
        'variables',
        '$location',
        '$auth',
        function ($rootScope, $state, $stateParams, $http, $window, $timeout, variables, $location, $auth) {

            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;

            $rootScope.$on('$stateChangeSuccess', function () {
                // scroll view to top
                $("html, body").animate({
                    scrollTop: 0
                }, 200);

                $timeout(function () {
                    $rootScope.pageLoading = false;
                    $($window).resize();
                }, 300);

                $timeout(function () {
                    $rootScope.pageLoaded = true;
                    $rootScope.appInitialized = true;
                    // wave effects
                    $window.Waves.attach('.md-btn-wave,.md-fab-wave', ['waves-button']);
                    $window.Waves.attach('.md-btn-wave-light,.md-fab-wave-light', ['waves-button', 'waves-light']);
                }, 600);

            });

            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

                // main search
                $rootScope.mainSearchActive = false;
                // single card
                $rootScope.headerDoubleHeightActive = false;
                // top bar
                $rootScope.toBarActive = false;
                // page heading
                $rootScope.pageHeadingActive = false;
                // top menu
                $rootScope.topMenuActive = false;
                // full header
                $rootScope.fullHeaderActive = false;
                // full height
                $rootScope.page_full_height = false;
                // secondary sidebar
                $rootScope.sidebar_secondary = false;
                $rootScope.secondarySidebarHiddenLarge = false;
                // footer
                $rootScope.footerActive = false;

                if ($($window).width() < 1220) {
                    // hide primary sidebar
                    $rootScope.primarySidebarActive = false;
                    $rootScope.hide_content_sidebar = false;
                }
                if (!toParams.hasOwnProperty('hidePreloader')) {
                    $rootScope.pageLoading = true;
                    $rootScope.pageLoaded = false;
                }

            });

            // fastclick (eliminate the 300ms delay between a physical tap and the firing of a click event on mobile browsers)
            FastClick.attach(document.body);

            // get version from package.json
            $http.get('./package.json').success(function (response) {
                $rootScope.appVer = response.version;
            });

            // modernizr
            $rootScope.Modernizr = Modernizr;

            // get window width
            var w = angular.element($window);
            $rootScope.largeScreen = w.width() >= 1220;

            w.on('resize', function () {
                return $rootScope.largeScreen = w.width() >= 1220;
            });

            // show/hide main menu on page load
            $rootScope.primarySidebarOpen = ($rootScope.largeScreen) ? true : false;

            $rootScope.pageLoading = true;

            // wave effects
            $window.Waves.init();

        }
    ])
    .run([
        'PrintToConsole',
        '$auth',
        '$rootScope',
        '$state',
        function (PrintToConsole, $auth, $rootScope, $state) {
            // app debug
            PrintToConsole.active = false;
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
                //console.log(fromState.name);
                //console.log(toState.name);
                if (!$auth.isAuthenticated() && toState.name != 'login' && toState.name != 'authcallbacklogin' && toState.name != 'authcallbacklogout') {
                    event.preventDefault();
                    console.log("Session Expired");
                    if (fromState.name != 'login' && fromState.name != 'authcallbacklogin' && fromState.name != 'authcallbacklogout') {
                        $state.go('login');
                    }
                    else {
                        $state.reload();
                    }
                }
            });
        }
    ])
;
