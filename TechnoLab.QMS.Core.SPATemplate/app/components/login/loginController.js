angular
    .module('spaBase')
    .controller('loginCtrl', [
        '$scope',
        '$rootScope',
        '$auth',
        '$state',
        '$localStorage',
        '$timeout',
        function ($scope, $rootScope, $auth, $state, $localStorage, $timeout) {

                $rootScope.$on('oidcauth:loggedIn', function(e) {
                        console.log('[EventCallback]', 'Event', e.name, e);
                        console.log('[EventCallback]', '$auth.isAuthenticated', $auth.isAuthenticated());
                });

                //$auth.signIn();

                $scope.login = function() {
                        $auth.signIn();
                };

                $scope.clear = function() {
                        $localStorage.$reset();
                };
        }
    ]);