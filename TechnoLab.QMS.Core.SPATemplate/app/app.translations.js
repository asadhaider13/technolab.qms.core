/**
 * Created by Atir on 15/01/2017.
 */
(function(){
    'use strict';
    var module = angular.module("spaBase");

    module.config(function ($translateProvider) {

        // english
        $translateProvider.translations('en', {
            'APP_TITLE': 'SPA TEMPLATE',

        });

        $translateProvider.preferredLanguage('en');

    });

}());
