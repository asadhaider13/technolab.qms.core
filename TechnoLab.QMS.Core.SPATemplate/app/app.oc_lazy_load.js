/* ocLazyLoad config */

spaBase
    .config([
        '$ocLazyLoadProvider',
        function($ocLazyLoadProvider) {
            var baseURI = 'http://localhost:37064/';
            $ocLazyLoadProvider.config({
                debug: false,
                events: false,
                modules: [
                    // ----------- UIKIT ------------------
                    {
                        name: 'lazy_uikit',
                        files: [
                            // uikit core
                            baseURI + "bower_components/uikit/js/uikit.js",
                            // uikit components
                            baseURI + "bower_components/uikit/js/components/accordion.js",
                            baseURI + "bower_components/uikit/js/components/autocomplete.js",
                            baseURI + "assets/js/custom/uikit_datepicker.js",
                            baseURI + "bower_components/uikit/js/components/form-password.js",
                            baseURI + "bower_components/uikit/js/components/form-select.js",
                            baseURI + "bower_components/uikit/js/components/grid.js",
                            baseURI + "bower_components/uikit/js/components/lightbox.js",
                            baseURI + "bower_components/uikit/js/components/nestable.js",
                            baseURI + "bower_components/uikit/js/components/notify.js",
                            baseURI + "bower_components/uikit/js/components/slideshow.js",
                            baseURI + "bower_components/uikit/js/components/sortable.js",
                            baseURI + "bower_components/uikit/js/components/sticky.js",
                            baseURI + "bower_components/uikit/js/components/tooltip.js",
                            baseURI + "assets/js/custom/uikit_timepicker.js",
                            baseURI + "bower_components/uikit/js/components/upload.js",
                            baseURI + "assets/js/custom/uikit_beforeready.js"
                        ],
                        serie: true
                    },

                    // ----------- FORM ELEMENTS -----------
                    {
                        name: 'lazy_autosize',
                        files: [
                            baseURI + 'bower_components/autosize/dist/autosize.js',
                            baseURI + 'app/modules/angular-autosize.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_iCheck',
                        files: [
                            baseURI + "bower_components/iCheck/icheck.min.js",
                            baseURI + 'app/modules/angular-icheck.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_selectizeJS',
                        files: [
                            baseURI + 'bower_components/selectize/dist/js/standalone/selectize.min.js',
                            baseURI + 'app/modules/angular-selectize.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_switchery',
                        files: [
                            baseURI + 'bower_components/switchery/dist/switchery.js',
                            baseURI + 'app/modules/angular-switchery.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_ionRangeSlider',
                        files: [
                            baseURI + 'bower_components/ion.rangeslider/js/ion.rangeSlider.min.js',
                            baseURI + 'app/modules/angular-ionRangeSlider.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_masked_inputs',
                        files: [
                             baseURI + 'bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js'
                        ]
                    },
                    {
                        name: 'lazy_character_counter',
                        files: [
                            baseURI + 'app/modules/angular-character-counter.js'
                        ]
                    },
                    {
                        name: 'lazy_parsleyjs',
                        files: [
                            baseURI + 'assets/js/custom/parsleyjs_config.js',
                            baseURI + 'bower_components/parsleyjs/dist/parsley.min.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_wizard',
                        files: [
                            baseURI + 'assets/js/custom/parsleyjs_config.js',
                            baseURI + 'bower_components/parsleyjs/dist/parsley.min.js',
                            baseURI + 'bower_components/angular-wizard/dist/angular-wizard.min.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_ckeditor',
                        files: [
                            baseURI + 'bower_components/ckeditor/ckeditor.js',
                            baseURI + 'app/modules/angular-ckeditor.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_tinymce',
                        files: [
                            baseURI + 'bower_components/tinymce/tinymce.min.js',
                            baseURI + 'app/modules/angular-tinymce.js'
                        ],
                        serie: true
                    },

                    // ----------- CHARTS -----------
                    {
                        name: 'lazy_charts_chartist',
                        files: [
                            baseURI + 'bower_components/chartist/dist/chartist.min.css',
                            baseURI + 'bower_components/chartist/dist/chartist.min.js',
                            baseURI + 'app/modules/angular-chartist.js'
                        ],
                        insertBefore: '#main_stylesheet',
                        serie: true
                    },
                    {
                        name: 'lazy_charts_easypiechart',
                        files: [
                            baseURI + 'bower_components/jquery.easy-pie-chart/dist/angular.easypiechart.min.js'
                        ]
                    },
                    {
                        name: 'lazy_charts_metricsgraphics',
                        files: [
                            baseURI + 'bower_components/metrics-graphics/dist/metricsgraphics.css',
                            baseURI + 'bower_components/d3/d3.min.js',
                            baseURI + 'bower_components/metrics-graphics/dist/metricsgraphics.min.js',
                            baseURI + 'app/modules/angular-metrics-graphics.js'
                        ],
                        insertBefore: '#main_stylesheet',
                        serie: true
                    },
                    {
                        name: 'lazy_charts_c3',
                        files: [
                            baseURI + 'bower_components/c3js-chart/c3.min.css',
                            baseURI + 'bower_components/d3/d3.min.js',
                            baseURI + 'bower_components/c3js-chart/c3.min.js',
                            baseURI + 'bower_components/c3-angular/c3-angular.min.js'
                        ],
                        insertBefore: '#main_stylesheet',
                        serie: true
                    },
                    {
                        name: 'lazy_charts_peity',
                        files: [
                            baseURI + 'bower_components/peity/jquery.peity.min.js',
                            baseURI + 'app/modules/angular-peity.js'
                        ],
                        serie: true
                    },

                    // ----------- COMPONENTS -----------
                    {
                        name: 'lazy_countUp',
                        files: [
                            baseURI + 'bower_components/countUp.js/countUp.js',
                            baseURI + 'app/modules/angular-countUp.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_clndr',
                        files: [
                            baseURI + 'bower_components/clndr/clndr.min.js',
                            baseURI + 'bower_components/angular-clndr/angular-clndr.min.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_google_maps',
                        files: [
                            baseURI + 'bower_components/ngmap/build/scripts/ng-map.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_weathericons',
                        files: [
                            baseURI + 'bower_components/weather-icons/css/weather-icons.css'
                        ],
                        insertBefore: '#main_stylesheet',
                        serie: true
                    },
                    {
                        name: 'lazy_prismJS',
                        files: [
                            baseURI + "bower_components/prism/prism.js",
                            baseURI + "bower_components/prism/components/prism-php.js",
                            baseURI + "bower_components/prism/plugins/line-numbers/prism-line-numbers.js",
                            baseURI + 'app/modules/angular-prism.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_dragula',
                        files: [
                            baseURI + 'bower_components/angular-dragula/dist/angular-dragula.min.js'
                        ]
                    },
                    {
                        name: 'lazy_pagination',
                        files: [
                            baseURI + 'bower_components/angularUtils-pagination/dirPagination.js'
                        ]
                    },
                    {
                        name: 'lazy_diff',
                        files: [
                            baseURI + 'bower_components/jsdiff/diff.min.js'
                        ]
                    },

                    // ----------- PLUGINS -----------
                    {
                        name: 'lazy_fullcalendar',
                        files: [
                            baseURI + 'bower_components/fullcalendar/dist/fullcalendar.min.css',
                            baseURI + 'bower_components/fullcalendar/dist/fullcalendar.min.js',
                            baseURI + 'bower_components/fullcalendar/dist/gcal.js',
                            baseURI + 'bower_components/angular-ui-calendar/src/calendar.js'
                        ],
                        insertBefore: '#main_stylesheet',
                        serie: true
                    },
                    {
                        name: 'lazy_codemirror',
                        files: [
                            baseURI + "bower_components/codemirror/lib/codemirror.css",
                            baseURI + "assets/css/codemirror_themes.min.css",
                            baseURI + "bower_components/codemirror/lib/codemirror.js",
                            baseURI + "assets/js/custom/codemirror_fullscreen.min.js",
                            baseURI + "bower_components/codemirror/addon/edit/matchtags.js",
                            baseURI + "bower_components/codemirror/addon/edit/matchbrackets.js",
                            baseURI + "bower_components/codemirror/addon/fold/xml-fold.js",
                            baseURI + "bower_components/codemirror/mode/htmlmixed/htmlmixed.js",
                            baseURI + "bower_components/codemirror/mode/xml/xml.js",
                            baseURI + "bower_components/codemirror/mode/php/php.js",
                            baseURI + "bower_components/codemirror/mode/clike/clike.js",
                            baseURI + "bower_components/codemirror/mode/javascript/javascript.js",
                            baseURI + "app/modules/angular-codemirror.js"
                        ],
                        insertBefore: '#main_stylesheet',
                        serie: true
                    },
                    {
                        name: 'lazy_datatables',
                        files: [
                            baseURI + 'bower_components/datatables/media/js/jquery.dataTables.min.js',
                            baseURI + 'bower_components/datatables-colvis/js/dataTables.colVis.js',
                            baseURI + 'bower_components/datatables-tabletools/js/dataTables.tableTools.js',
                            baseURI + 'bower_components/angular-datatables/dist/angular-datatables.js',
                            baseURI + 'assets/js/custom/jquery.dataTables.columnFilter.js',
                            baseURI + 'bower_components/angular-datatables/dist/plugins/columnfilter/angular-datatables.columnfilter.min.js',
                            baseURI + 'assets/js/custom/datatables_uikit.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_gantt_chart',
                        files: [
                            
                            baseURI + 'bower_components/jquery-ui/ui/minified/core.min.js',
                        baseURI + 'bower_components/jquery-ui/ui/minified/widget.min.js',
                        baseURI + 'bower_components/jquery-ui/ui/minified/mouse.min.js',
                        baseURI + 'bower_components/jquery-ui/ui/minified/resizable.min.js',
                        baseURI + 'bower_components/jquery-ui/ui/minified/draggable.min.js',
                            
                            baseURI + 'assets/js/custom/gantt_chart.min.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_tablesorter',
                        files: [
                            baseURI + 'bower_components/tablesorter/dist/js/jquery.tablesorter.min.js',
                            baseURI + 'bower_components/tablesorter/dist/js/jquery.tablesorter.widgets.min.js',
                            baseURI + 'bower_components/tablesorter/dist/js/widgets/widget-alignChar.min.js',
                            baseURI + 'bower_components/tablesorter/dist/js/extras/jquery.tablesorter.pager.min.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_vector_maps',
                        files: [
                            baseURI + 'bower_components/raphael/raphael-min.js',
                            baseURI + 'bower_components/jquery-mapael/js/jquery.mapael.js',
                            baseURI + 'bower_components/jquery-mapael/js/maps/world_countries.js',
                            baseURI + 'bower_components/jquery-mapael/js/maps/usa_states.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_dropify',
                        files: [
                            baseURI + 'assets/skins/dropify/css/dropify.css',
                            baseURI + 'assets/js/custom/dropify/dist/js/dropify.min.js'
                        ],
                        insertBefore: '#main_stylesheet'
                    },
                    {
                        name: 'lazy_tree',
                        files: [
                            baseURI + 'assets/js/custom/easytree/skin-material/ui.easytree.min.css',
                            baseURI + 'assets/js/custom/easytree/jquery.easytree.min.js'
                        ],
                        insertBefore: '#main_stylesheet',
                        serie: true
                    },

                    // ----------- KENDOUI COMPONENTS -----------
                    {
                        name: 'lazy_KendoUI',
                        files: [
                            baseURI + 'bower_components/kendo-ui/js/kendo.core.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.color.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.data.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.calendar.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.popup.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.datepicker.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.timepicker.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.datetimepicker.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.list.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.fx.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.userevents.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.menu.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.draganddrop.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.slider.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.mobile.scroller.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.autocomplete.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.combobox.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.dropdownlist.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.colorpicker.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.combobox.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.maskedtextbox.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.multiselect.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.numerictextbox.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.toolbar.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.panelbar.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.window.min.js',
                            baseURI + 'bower_components/kendo-ui/js/kendo.angular.min.js',
                            baseURI + 'bower_components/kendo-ui/styles/kendo.common-material.min.css',
                            baseURI + 'bower_components/kendo-ui/styles/kendo.material.min.css'
                        ],
                        insertBefore: '#main_stylesheet',
                        serie: true
                    },

                    // ----------- UIKIT HTMLEDITOR -----------
                    {
                        name: 'lazy_htmleditor',
                        files: [
                            baseURI + "bower_components/codemirror/lib/codemirror.js",
                            baseURI + "bower_components/codemirror/mode/markdown/markdown.js",
                            baseURI + "bower_components/codemirror/addon/mode/overlay.js",
                            baseURI + "bower_components/codemirror/mode/javascript/javascript.js",
                            baseURI + "bower_components/codemirror/mode/php/php.js",
                            baseURI + "bower_components/codemirror/mode/gfm/gfm.js",
                            baseURI + "bower_components/codemirror/mode/xml/xml.js",
                            baseURI + "bower_components/marked/lib/marked.js",
                            baseURI + "bower_components/uikit/js/components/htmleditor.js"
                        ],
                        serie: true
                    },

                    // ----------- STYLE SWITCHER -----------
                    {
                        name: 'lazy_style_switcher',
                        files: [
                            baseURI + "app/shared/style_switcher/style_switcher.js"
                        ]
                    }

                ]
            })
        }
    ]);